/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Word {

    private Integer id;
    private String word;
    private String hash;

    public Word(Integer id, String word, String hash) {
        this.id = id;
        this.word = word;
        this.hash = hash;
    }

    public Integer getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getHash() {
        return hash;
    }

    @Override
    public String toString() {
        return "Word {" + "word=" + word + ", hash=" + hash + '}';
    }

}
