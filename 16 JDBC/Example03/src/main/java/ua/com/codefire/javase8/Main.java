/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;
import ua.com.codefire.javase8.models.Word;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    private static final String SQL_SELECT_QUERY = "SELECT * FROM `rainbow` WHERE LENGTH(`word`) BETWEEN 3 AND 5";

    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection("jdbc:sqlite:database.sl3")) {
            ResultSet rs = conn.createStatement().executeQuery(SQL_SELECT_QUERY);

            StreamSupport.stream(new Spliterators.AbstractSpliterator<Word>(Integer.MAX_VALUE, Spliterator.ORDERED) {
                @Override
                public boolean tryAdvance(Consumer<? super Word> action) {
                    try {
                        if (!rs.next()) {
                            return false;
                        }
                        action.accept(new Word(rs.getInt("id"), rs.getString("word"), rs.getString("md5hash")));
                    } catch (SQLException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return true;
                }
            }, false).forEach(System.out::println);

        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
