/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.apache.commons.codec.digest.DigestUtils;
import ua.com.codefire.javase8.models.Word;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    private static final String SQL_UPDATE_QUERY = "UPDATE `rainbow` SET `sha1hash` = ?, `sha256hash` = ?, `sha384hash` = ?, `sha512hash` = ? WHERE id = ?";

    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection("jdbc:sqlite:database.sl3")) {
            ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM `rainbow`");

            // ADDING ADDITIONAL COLLUMNS
//            try (Statement statement = conn.createStatement()) {
//                new BufferedReader(new InputStreamReader(Main.class.getClass().getResourceAsStream("/alter.sql"))).lines()
//                        .forEach(query -> {
//                            try {
//                                statement.execute(query);
//                            } catch (SQLException ex) {
//                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//                        });
//            }
            // /ADDING ADDITIONAL COLLUMNS
            List<Word> rainbow = StreamSupport.stream(new Spliterators.AbstractSpliterator<Word>(Integer.MAX_VALUE, Spliterator.ORDERED) {
                @Override
                public boolean tryAdvance(Consumer<? super Word> action) {
                    try {
                        if (!rs.next()) {
                            return false;
                        }
                        Word word = new Word(rs.getInt("id"), rs.getString("word"));
                        word.setMd5Hash(rs.getString("md5hash"));
                        action.accept(word);
                    } catch (SQLException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return true;
                }
            }, false).collect(Collectors.toList());

            try (PreparedStatement ps = conn.prepareStatement(SQL_UPDATE_QUERY)) {
                drawSeparatorLine();
                System.out.printf("| %s | %12s | %-32s | %-40s | %-64s | %-96s | %-128s |\n",
                        "#ID", "WORD", "MD5", "SHA1", "SHA256", "SHA384", "SHA512");
                drawSeparatorLine();

                rainbow.stream()
                        .peek((w) -> {
                            w.setSha1Hash(DigestUtils.sha1Hex(w.getWord()));
                            w.setSha256Hash(DigestUtils.sha256Hex(w.getWord()));
                            w.setSha384Hash(DigestUtils.sha384Hex(w.getWord()));
                            w.setSha512Hash(DigestUtils.sha512Hex(w.getWord()));
                        })
                        .forEach((w) -> {
                            System.out.printf("| %3d | %12s | %s | %s | %s | %s | %s |\n",
                                    w.getId(), w.getWord(), w.getMd5Hash(), w.getSha1Hash(), w.getSha256Hash(), w.getSha384Hash(), w.getSha512Hash());
                            try {
                                ps.setString(1, w.getSha1Hash());
                                ps.setString(2, w.getSha256Hash());
                                ps.setString(3, w.getSha384Hash());
                                ps.setString(4, w.getSha512Hash());
                                ps.setInt(5, w.getId());
                                ps.executeUpdate();
                            } catch (SQLException ex) {
                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        });
                drawSeparatorLine();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Printing separator line.
     */
    private static void drawSeparatorLine() {
        System.out.print("+");
        for (int i = 0; i < 197; i++) {
            System.out.print("-%");
        }
        System.out.println("-+");
    }
}
