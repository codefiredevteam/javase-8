/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    private static final String SQL_DROP_QUERY = "DROP TABLE IF EXISTS `rainbow`";

    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection("jdbc:sqlite:database.sl3")) {
            String tableStructureQuery = IOUtils.toString(Main.class.getResourceAsStream("/structure.sql"), "UTF-8");
            String tableDataQuery = IOUtils.toString(Main.class.getResourceAsStream("/data.sql"), "UTF-8");

            try (Statement statement = conn.createStatement()) {
                statement.execute(SQL_DROP_QUERY);
                statement.execute(tableStructureQuery);
                
                int affectedRows = statement.executeUpdate(tableDataQuery);
                System.out.printf("Affected rows: %03d\n", affectedRows);
            }
        } catch (SQLException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
