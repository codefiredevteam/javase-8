/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        Path store = Paths.get(System.getProperty("user.home"), "Downloads");

        try {
            Files.find(store, 8, (Path p, BasicFileAttributes bfa) -> Files.isRegularFile(p))
                    //            Files.list(store)
                    //                    .filter(p -> !Files.isDirectory(p))
                    .parallel()
                    .collect(Collectors.groupingBy((Path path) -> {
                        try {
                            return DigestUtils.sha512Hex(Files.newInputStream(path));
                        } catch (IOException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        return null;
                    }))
                    .forEach((String hash, List<Path> paths) -> {
                        if (paths.size() > 1) {
                            System.err.printf("%s:\n%s\n", hash, paths);
                        } else {
                            System.out.printf("%s:\n%s\n", hash, paths);
                        }
                    });
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
