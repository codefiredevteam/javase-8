/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import ua.com.codefire.javase8.models.air.AirBus;
import ua.com.codefire.javase8.models.air.Boeing;
import ua.com.codefire.javase8.models.land.Auto;
import ua.com.codefire.javase8.models.land.Bus;
import ua.com.codefire.javase8.models.land.Truck;
import ua.com.codefire.javase8.models.water.AircraftCarrierShip;
import ua.com.codefire.javase8.models.water.CruiseShip;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {
    
    public static void main(String[] args) {
        
        System.out.println(":: SHIPS");
        
        CruiseShip balmoral = new CruiseShip(1778, 471);
        
        AircraftCarrierShip hancock = new AircraftCarrierShip(60, 1200);
        
        System.out.println(balmoral);
        System.out.println(hancock);
        
        System.out.println();
        // --=--=--=--=--=--=--=--=--=--=--=--=--=--=--=--=
        System.out.println(":: CARS");
        
        Auto bugattiVeyron = new Auto(7.993, 1020, 407);
        
        Bus ikarus256 = new Bus("Patis - Noterdam", 45);
        
        Truck liebherrT282B = new Truck(363000, 4);
        
        System.out.println(bugattiVeyron);
        System.out.println(ikarus256);
        System.out.println(liebherrT282B);
        
        System.out.println();
        // --=--=--=--=--=--=--=--=--=--=--=--=--=--=--=--=
        System.out.println(":: PLANES");
        
        AirBus airBusA320 = new AirBus(180, 20);
        
        Boeing boeing747 = new Boeing(818, 23);
        
        System.out.println(airBusA320);
        System.out.println(boeing747);
    }
}
