/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models.water;

import ua.com.codefire.javase8.models.Transport;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class AircraftCarrierShip extends Ship{
    
    private int numberOfAircrafts;
    private int numberOfCrew;

    public AircraftCarrierShip(int numberOfAircrafts, int numberOfCrew) {
        super(Transport.TYPE_CARGO);
        this.numberOfAircrafts = numberOfAircrafts;
        this.numberOfCrew = numberOfCrew;
    }

    public int getNumberOfAircrafts() {
        return numberOfAircrafts;
    }

    public int getNumberOfCrew() {
        return numberOfCrew;
    }

    @Override
    public String toString() {
        return "AircraftCarrierShip{" + "numberOfAircrafts=" + numberOfAircrafts + ", numberOfCrew=" + numberOfCrew + '}';
    }
}
