/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models.products;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public abstract class Drink {
    
    public static final String DRINK_COFFEE_RISTRETTO = "RISTRETTO";
    public static final String DRINK_COFFEE_ESPRESSO = "ESPRESSO";
    public static final String DRINK_COFFEE_AMERICANO = "AMERICANO";
    public static final String DRINK_JUICE_APPLE = "APPLE JUICE";
    public static final String DRINK_JUICE_ORANGE = "ORANGE JUICE";
    public static final String DRINK_JUICE_BERRY = "BERRY JUICE";
    public static final String DRINK_TEA_BLACK = "BLACK TEA";
    public static final String DRINK_TEA_GREEN = "GREEN TEA";

    private int volume;

    public Drink(int volume) {
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

}
