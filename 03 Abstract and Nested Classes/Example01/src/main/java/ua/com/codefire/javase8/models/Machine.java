/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

import ua.com.codefire.javase8.models.products.Drink;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public abstract class Machine {

    private boolean powerState;

    public Machine() {
    }

    public boolean isPowerState() {
        return powerState;
    }

    /**
     * Toggle powerState of machine.
     */
    public void toggleState() {
        // Reverse powerState value
        powerState = !powerState;

        System.out.printf("Machine is turned %s\n", powerState ? "ON" : "OFF");
    }

    public abstract Cup make(String drinkName);

    /**
     * Static Nested Class.
     */
    public static class Cup {

        private Drink drink;

        public Cup() {
        }

        public Drink getDrink() {
            return drink;
        }

        public void setDrink(Drink drink) {
            this.drink = drink;
        }

        @Override
        public String toString() {
            return "Cup{" + "drink=" + drink + '}';
        }
    }
}
