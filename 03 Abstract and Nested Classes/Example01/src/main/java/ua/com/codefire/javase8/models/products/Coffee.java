/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models.products;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Coffee extends Drink {

    private int arabica;
    private int arabusta;

    public Coffee(int arabica, int arabusta, int volume) {
        super(volume);
        this.arabica = arabica;
        this.arabusta = arabusta;
    }

    public int getArabica() {
        return arabica;
    }

    public int getArabusta() {
        return arabusta;
    }

    @Override
    public String toString() {
        return "Coffee{" + "arabica=" + arabica + ", arabusta=" + arabusta + '}';
    }
}
