/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import ua.com.codefire.javase8.models.Machine;
import ua.com.codefire.javase8.models.Machine.Cup;
import ua.com.codefire.javase8.models.VendorMachine;
import ua.com.codefire.javase8.models.products.Drink;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        
        Machine machine01 = new VendorMachine();
        
        Cup cupEspresso = machine01.make(Drink.DRINK_COFFEE_ESPRESSO);
        System.out.println(cupEspresso);
        
        machine01.toggleState();
        
        cupEspresso = machine01.make(Drink.DRINK_COFFEE_ESPRESSO);
        System.out.println(cupEspresso);
        
        Cup cupTea = machine01.make(Drink.DRINK_TEA_GREEN);
        System.out.println(cupTea);
        
        cupTea = machine01.make(Drink.DRINK_JUICE_APPLE);
        System.out.println(cupTea);
        
        machine01.toggleState();
    }
}
