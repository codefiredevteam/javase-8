/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

import ua.com.codefire.javase8.models.products.Coffee;
import ua.com.codefire.javase8.models.products.Drink;
import ua.com.codefire.javase8.models.products.Juice;
import ua.com.codefire.javase8.models.products.Tea;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class VendorMachine extends Machine {

    @Override
    public Cup make(String drink) {
        if (isPowerState()) {
            Cup cup = new Cup();

            switch (drink) {
                case Drink.DRINK_COFFEE_RISTRETTO:
                    cup.setDrink(new Coffee(70, 30, 20));
                    break;
                case Drink.DRINK_COFFEE_ESPRESSO:
                    cup.setDrink(new Coffee(60, 40, 40));
                    break;
                case Drink.DRINK_COFFEE_AMERICANO:
                    cup.setDrink(new Coffee(50, 50, 80));
                    break;
                case Drink.DRINK_JUICE_APPLE:
                    cup.setDrink(new Juice("Apple", 150));
                    break;
                case Drink.DRINK_JUICE_ORANGE:
                    cup.setDrink(new Juice("Orange", 150));
                    break;
                case Drink.DRINK_JUICE_BERRY:
                    cup.setDrink(new Juice("Berry", 150));
                    break;
                case Drink.DRINK_TEA_BLACK:
                    cup.setDrink(new Tea("Morning Black", 210));
                    break;
                case Drink.DRINK_TEA_GREEN:
                    cup.setDrink(new Tea("East Green", 210));
                    break;
            }

            return cup;
        }

        System.out.println("Machine is OFF (note: Turn ON before using).");
        return null;
    }

}
