/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {

        Locale.setDefault(new Locale("ru","RU"));
        
        ResourceBundle bundle = PropertyResourceBundle.getBundle("messages");

        try (Scanner scan = new Scanner(System.in)) {
            System.out.println(bundle.getString("msg.welcome"));
            System.out.println(bundle.getString("msg.menu"));
            System.out.println(bundle.getString("msg.menu.item.1"));
            System.out.println(bundle.getString("msg.menu.item.2"));
            System.out.println(bundle.getString("msg.menu.item.3"));
            System.out.println(bundle.getString("msg.menu.item.exit"));

            while (scan.hasNextLine()) {
                String input = scan.nextLine();

                switch (input.toLowerCase()) {
                    case "1":
                        System.out.println(input);
                        break;
                    case "2":
                        LocalTime time = LocalTime.now();
                        System.out.println(time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)));
                        break;
                    case "3":
                        LocalDate date = LocalDate.now();
//                        System.out.println(date.format(DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.getDefault())));
                        System.out.println(date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
                        break;
                    case "0":
                        System.out.println(bundle.getString("msg.goodbye"));
                        return;

                }
            }
        }

    }
}
