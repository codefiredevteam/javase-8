/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.Consumer;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) throws IOException {
        Path sources = Paths.get("./").normalize().toAbsolutePath();

        Consumer<Path> printer = path -> System.out.println("  " + path);

        System.out.println("XML");
        Files.find(sources, 11, (Path path, BasicFileAttributes attr) -> path.getFileName().toString().endsWith(".xml"))
                .forEach(printer);

        System.out.println("Java");
        Files.find(sources, 11, (Path path, BasicFileAttributes attr) -> path.getFileName().toString().endsWith(".java"))
                .forEach(printer);

        System.out.println("Class");
        Files.find(sources, 11, (Path path, BasicFileAttributes attr) -> path.getFileName().toString().endsWith(".class"))
                .forEach(printer);
    }
}
