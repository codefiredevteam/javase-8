/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) throws IOException {///src//ua/com/codefire/javase8
        Path path = Paths.get("./", "src", "main", "java", "ua", "com", "codefire", "javase8");
        System.out.println("Builded: " + path);
        System.out.println("Normalize: " + path.normalize());
        System.out.println("Absolute: " + path.toAbsolutePath());
        System.out.println("URI (open in browser): " + path.toUri());
        System.out.println(path.getParent());
        System.out.println();

        Path source = Paths.get(path.toString(), "Main.java");
        System.out.println(source);
        System.out.printf("Readable: %b, Writable: %b, Executable: %b\n",
                Files.isReadable(source), Files.isWritable(source),
                Files.isExecutable(source));
        System.out.printf("Java code bytes: %d bytes\n", Files.size(source));

        String src = "./";
        String dst = "../target/";
        
//        new File("./").toPath().toFile().toPath().toFile().toPath().toFile().toPath().toFile().toPath().toFile();

        if (!Files.exists(Paths.get(dst))) {
            Files.createDirectory(Paths.get(dst));
        }

        System.out.println("List of sub files/dirs:");
        Files.list(Paths.get(src))
                .filter((Path p) -> !Files.isDirectory(p))
                .filter((Path p) -> {
                    try {
                        Files.copy(p, Paths.get(dst, p.getFileName().toString()), StandardCopyOption.REPLACE_EXISTING);
                        return true;
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return false;
                })
                .map((Path p) -> Paths.get(dst, p.getFileName().toString()))
                .forEach(System.out::println);
    }
}
