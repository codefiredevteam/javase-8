/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        
        try (BufferedReader br = new BufferedReader(new FileReader(new File("./lipsum.txt")))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        List<String> words = Arrays.asList(sb.toString().split("[\\.,\\s!?]+"));
        
        Map<String, Integer> counter = new HashMap<>();
        
        for (String w : words) {
            if (counter.containsKey(w)) {
                counter.put(w, counter.get(w) + 1);
            } else {
                counter.put(w, 1);
            }
        }
        
        int total = 0;
        for (String w : counter.keySet()) {
            total += counter.get(w);
            System.out.printf("%-15s = [%03d]\n", w, counter.get(w));
        }
        System.out.println("=---------------------=");
        System.out.printf("%-15s = [%03d]\n", "Total", total);
    }
}
