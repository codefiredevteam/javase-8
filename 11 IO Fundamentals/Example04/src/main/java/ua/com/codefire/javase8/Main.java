/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        try {
            byte bWrite[] = {65, 66, 67, 68, 69};
            
            try (OutputStream os = new FileOutputStream("test.txt")) {
                for (int x = 0; x < bWrite.length; x++) {
                    os.write(bWrite[x]); // writes the bytes
                }
            }

            try (InputStream is = new FileInputStream("test.txt")) {
                int size = is.available();
                
//                for (int i = 0; i < is.available(); i++) {
                for (int i = 0; i < size; i++) {
                    System.out.print((char) is.read() + "  ");
                }
            }
        } catch (IOException e) {
            System.out.print("Exception");
        }
    }
}
