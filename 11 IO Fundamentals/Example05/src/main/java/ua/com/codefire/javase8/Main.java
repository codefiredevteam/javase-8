/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    
    static {
        try {
            LOG.addHandler(new FileHandler("Java", 100, 10));
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        System.out.println("Enter some text:");
        
        try (Scanner scanner = new Scanner(System.in); FileWriter fw = new FileWriter(new File("data.txt"))) {
            String enter;
            
            while (!"exit".equals(enter = scanner.nextLine())) {
                fw.append(enter).write('\n');
            }
            
            throw  new IOException("OKOKO");
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
}
