/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.io.File;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        File file = null;
        File[] paths;

        try {
            // create new file object
            file = new File("/tmp");

            // array of files and directory
            paths = file.listFiles();

            if (paths != null) { // for each name in the path array
                for (File sub : paths) {
                    // prints filename and directory name
                    System.out.printf("[%s] %s\n", sub.isFile() ? "F" : "D", sub.getName());
                }
            }
        } catch (Exception e) {
            // if any error occurs
            e.printStackTrace();
        }
    }
}
