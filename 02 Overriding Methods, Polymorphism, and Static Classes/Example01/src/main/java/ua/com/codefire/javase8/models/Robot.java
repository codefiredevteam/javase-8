/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Robot {

    // Constant
    public static final String DEFAULT_STATION = "Popular Music FM";
    private static int count = 0;
    
    public static int getTotalRobots() {
        return count;
    }

    
    private String station;
    private String name;

    public Robot(String name) {
        this.name = name;
        this.station = DEFAULT_STATION;
        // Increse count
        count++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void charge() {
        System.out.println("Charging...");
        System.out.println("-");
        System.out.println("=");
        System.out.println("=-");
        System.out.println("==");
        System.out.println("==-");
        System.out.println("===");
        System.out.println("===-");
        System.out.println("Charged!");
    }

    public void playRadio() {
        System.out.println("Radio /ON");
        System.out.println("You may also change station");
    }

    public void changeStation(String newStation) {
        System.out.printf("%s -> %s\n", station, newStation);
        
        station = newStation;
    }

    public void stopRadio() {
        System.out.println("Radio /OFF");
    }

    @Override
    public String toString() {
        return "Robot {" + "name=" + name + '}';
    }

}
