/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Humanoid extends Robot {
    
    public Humanoid(String name) {
        super(name);
    }

    @Override
    public void charge() {
        // Super method invocation.
        //super.charge();
        System.out.println("HUMANOID CHARGING...");
        System.out.println("....................");
        System.out.println("...CHARGING COMPLETE");
    }

    public void cleanBedroom() {
        System.out.println("Bedroom clean");
    }

    public void cleanBathroom() {
        System.out.println("Bathroom clean");
    }

    public void cleanKitchen() {
        System.out.println("Kitchen clean");
    }

    @Override
    public String toString() {
        return "Humanoid{" + getName() + '}';
    }
}
