/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import ua.com.codefire.javase8.models.Humanoid;
import ua.com.codefire.javase8.models.Robot;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {

        Robot R2D2 = new Robot("R2D2");

        R2D2.charge();

        R2D2.playRadio();
        R2D2.changeStation("Rock FM");
        R2D2.stopRadio();

        Humanoid C3P0 = new Humanoid("C3P0");
        
        C3P0.cleanKitchen();
        C3P0.cleanBathroom();

        C3P0.charge();

        R2D2.playRadio();
        R2D2.changeStation("Classic FM");
        R2D2.stopRadio();

        System.out.println("Total robots created: " + Humanoid.getTotalRobots());
        System.out.println("Total humanoids created: " + Robot.getTotalRobots());
    }
}
