/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Computer {

    private Processor cpu;
    private Memory[] ram;
    private DiskDrive hdd;

    public Computer() {
    }

    public Processor getCpu() {
        return cpu;
    }

    public void setCpu(Processor cpu) {
        this.cpu = cpu;
    }

    public Memory[] getRam() {
        return ram;
    }

    public void setRam(Memory[] ram) {
        this.ram = ram;
    }

    public DiskDrive getHdd() {
        return hdd;
    }

    public void setHdd(DiskDrive hdd) {
        this.hdd = hdd;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Computer:");
        builder.append("\n    CPU: ").append(cpu);
        builder.append("\n    RAM: ");

        for (Memory memory : ram) {
            builder.append("\n       - ").append(memory);
        }

        builder.append("\n    HDD: ").append(hdd);
        return builder.toString();
    }

    /**
     * Static Nested Class
     */
    public static class Processor {

        private int numberOfCores;
        private double frequency;
        private boolean enabledTurboBoost;

        public Processor(int numberOfCores, double frequency) {
            this.numberOfCores = numberOfCores;
            this.frequency = frequency;
        }

        public int getNumberOfCores() {
            return numberOfCores;
        }

        public double getFrequency() {
            return enabledTurboBoost ? frequency * 1.3 : frequency;
        }

        public boolean isEnabledTurboBoost() {
            return enabledTurboBoost;
        }

        public void setEnabledTurboBoost(boolean enabledTurboBoost) {
            this.enabledTurboBoost = enabledTurboBoost;
        }

        @Override
        public String toString() {
            return "Processor{" + "numberOfCores=" + numberOfCores + ", frequency=" + getFrequency() + ", enabledTurboBoost=" + enabledTurboBoost + '}';
        }

    }

    /**
     * Non Static Nested Class.
     */
    public class Memory {

        private String type;
        private int capacity;
        private int frequency;

        public Memory(String type, int capacity, int frequency) {
            this.type = type;
            this.capacity = capacity;
            this.frequency = frequency;
        }

        public String getType() {
            return type;
        }

        public int getCapacity() {
            return capacity;
        }

        public int getFrequency() {
            return frequency;
        }

        @Override
        public String toString() {
            return "Memory{" + "type=" + type + ", capacity=" + capacity + ", frequency=" + frequency + "MHz}";
        }

    }
}
