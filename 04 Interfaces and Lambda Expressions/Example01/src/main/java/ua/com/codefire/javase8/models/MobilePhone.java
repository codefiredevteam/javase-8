/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

import java.util.Arrays;
import ua.com.codefire.javase8.models.futures.Call;
import ua.com.codefire.javase8.models.futures.MessageService;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class MobilePhone implements Call, MessageService {

    @Override
    public void callTo(String phoneNumber) {
        System.out.printf("Calling to %s \n", phoneNumber);
    }

    @Override
    public boolean sendSMS(String recipient, String text) {
        System.out.printf("Sending SMS [%s] to %s\n", text, recipient);
        return true;
    }

    @Override
    public boolean sendMMS(String recipient, byte[] image) {
        System.out.printf("Sending MMS with [%s] to %s\n", Arrays.toString(image), recipient);
        return true;
    }

}
