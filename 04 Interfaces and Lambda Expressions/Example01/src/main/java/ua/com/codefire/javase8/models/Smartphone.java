/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

import ua.com.codefire.javase8.models.futures.Application;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Smartphone extends MobilePhone implements Application {

    @Override
    public void run(String applicationName) {
        System.out.println("Running application: " + applicationName);
        System.out.println("...work with application...");
        System.out.println("Stopping application: " + applicationName);
    }
    
}
