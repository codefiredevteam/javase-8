/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import ua.com.codefire.javase8.models.CellPhone;
import ua.com.codefire.javase8.models.MobilePhone;
import ua.com.codefire.javase8.models.Smartphone;
import ua.com.codefire.javase8.models.futures.Call;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {

        CellPhone oldPhone = new CellPhone();
        MobilePhone nokia5510 = new MobilePhone();
        Smartphone iPhone6s = new Smartphone();

        Call[] callers = {
            oldPhone, nokia5510, iPhone6s
        };

        for (Call caller : callers) {
            System.out.println(caller.getClass().getSimpleName());
            caller.callTo("+380 (67) 123-45-67");
            System.out.println();
        }

        byte[] image = new byte[]{0, 1, 0, 1, 0};
        nokia5510.sendMMS("+380 (00) 123-12-23", image);

        iPhone6s.sendSMS("+380 (76) 321-54-76", "Hello World!");
        iPhone6s.run("Photos");
    }
}
