/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import ua.com.codefire.javase8.models.Cat;
import ua.com.codefire.javase8.models.AnimalShelter;
import ua.com.codefire.javase8.models.Dog;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {

        Cat cat1 = new Cat("Pusik");
        cat1.setAge(3);
        Cat cat2 = new Cat("Bahuba");
        cat2.setAge(5);

        AnimalShelter<Cat> catShelter = new AnimalShelter<>("Catty");
        catShelter.give(cat1);
        catShelter.give(cat2);

        catShelter.printInfo();

        AnimalShelter<Dog> dogShelter = new AnimalShelter<>("AllDogger");

        dogShelter.give(new Dog("Bobik", 2));
        dogShelter.give(new Dog("Kosya", 6));
        dogShelter.give(new Dog("Piter", 1));
        
        dogShelter.printInfo();

    }
}
