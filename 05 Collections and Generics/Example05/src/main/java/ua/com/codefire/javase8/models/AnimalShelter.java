/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 * @param <T>
 */
public class AnimalShelter<T extends Pet> {

    private String name;
    private List<T> pets;

    public AnimalShelter(String name) {
        this.name = name;
        this.pets = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public boolean give(T pet) {
        if (test(pet)) {
            return pets.add(pet);
        }

        return false;
    }

    public boolean take(T pet) {
        return pets.remove(pet);
    }

    public boolean test(T pet) {
        print(pet);

        System.out.println(" in good condition.");

        return true;
    }

    public void print(T pet) {
        System.out.println(pet);
    }

    public void printInfo() {
        System.out.println(name);
        
        for (T pet : pets) {
            System.out.println(pet);
        }
    }

}
