/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import ua.com.codefire.javase8.models.Dog;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        // Takes a list o Dog objects
        List<Dog> list = new ArrayList<>();

        list.add(new Dog("Shaggy", 3));
        list.add(new Dog("Lacy", 2));
        list.add(new Dog("Roger", 10));
        list.add(new Dog("Tommy", 4));
        list.add(new Dog("Tammy", 1));
        
        // Sorts the array list
        Collections.sort(list);// Sorts the array list

        //printing the sorted list of names
        for (Dog a : list) {
            System.out.print(a.getDogName() + ", ");
        }

        // Sorts the array list using comparator
        Collections.sort(list, (Dog d1, Dog d2) -> {
            return d1.getDogName().compareTo(d2.getDogName());
        });

        //printing the sorted list of ages
        System.out.println(" ");
        for (Dog a : list) {
            System.out.print(a.getDogName() + ": " + a.getDogAge() + ", ");
        }

    }
}
