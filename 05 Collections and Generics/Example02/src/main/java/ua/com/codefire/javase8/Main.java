/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        // Create and initialize linked list
        List ll = new LinkedList();
        ll.add(-8);
        ll.add(20);
        ll.add(-20);
        ll.add(8);
        
        ll = new ArrayList(ll);
        
        for (int i = 0; i < ll.size(); i++) {
            System.out.println(ll.get(i));
        }

        // Create a reverse order comparator
        Comparator r = Collections.reverseOrder();
        // Sort list by using the comparator
        Collections.sort(ll, r);
        // Get iterator
        Iterator li = ll.iterator();
        System.out.print("List sorted in reverse: ");
        while (li.hasNext()) {
            System.out.print(li.next() + " ");
        }
        System.out.println();
        Collections.shuffle(ll);
        // display randomized list
        li = ll.iterator();
        System.out.print("List shuffled: ");
        while (li.hasNext()) {
            System.out.print(li.next() + " ");
        }
        System.out.println();
        System.out.println("Minimum: " + Collections.min(ll));
        System.out.println("Maximum: " + Collections.max(ll));

    }
}
