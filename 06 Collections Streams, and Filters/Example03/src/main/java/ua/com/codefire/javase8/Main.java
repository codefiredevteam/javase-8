/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("Ukraine");
        System.out.printf("Total cities and towns: %d\n", Arrays.stream(cities).count());

//        Arrays.stream(cities)
//                .sorted(String::compareTo)
//                .collect(Collectors.groupingBy(name -> name.substring(0, 1)))
//                .forEach((letter, names) -> {
//                    System.out.printf("\n%s -~=-~=-~=-~=-~=-~=-~=-~=-~=\n", letter);
//                    names.forEach(name -> System.out.printf("  %02d. %s\n", names.indexOf(name) + 1, name));
//                });
        Arrays.stream(cities)
                .sorted(String::compareTo)
                .collect(Collectors.groupingBy(name -> name.substring(0, 1)))
                .entrySet().stream().filter(e -> e.getKey().matches("[A-I]"))
                .forEach(e -> {
                    System.out.printf("\n%s -~=-~=-~=-~=-~=-~=-~=-~=-~=\n", e.getKey());
                    e.getValue().forEach(name -> System.out.printf("  %02d. %s\n", e.getValue().indexOf(name) + 1, name));
                });
    }

    //<editor-fold defaultstate="collapsed" desc="Cities names">
    private static final String[] cities = {
        "Kiev", "Kharkiv", "Dnipropetrovsk", "Odessa", "Donetsk", "Zaporizhia", "Lviv", "Kryvyi Rih", "Mykolaiv", "Mariupol",
        "Luhansk", "Makiivka", "Vinnytsia", "Simferopol", "Sevastopol", "Kherson", "Poltava", "Chernihiv", "Cherkasy", "Sumy",
        "Horlivka", "Zhytomyr", "Kamianske", "Kropyvnytskyi", "Khmelnytskyi", "Rivne", "Chernivtsi", "Kremenchuk", "Ternopil", "Ivano-Frankivsk",
        "Lutsk", "Bila Tserkva", "Kramatorsk", "Melitopol", "Kerch", "Nikopol", "Sloviansk", "Berdiansk", "Sieverodonetsk", "Alchevsk",
        "Pavlohrad", "Uzhhorod", "Lysychansk", "Yevpatoria", "Yenakiieve", "Kamianets-Podilskyi", "Kostiantynivka", "Krasnyi Luch", "Oleksandriia", "Konotop",
        "Stakhanov", "Uman", "Berdychiv", "Shostka", "Brovary", "Izmail", "Bakhmut", "Mukacheve", "Yalta", "Drohobych",
        "Nizhyn", "Feodosiya", "Sverdlovsk", "Novomoskovsk", "Torez", "Chervonohrad", "Pervomaisk", "Smila", "Pokrovsk", "Kalush",
        "Korosten", "Kovel", "Rubizhne", "Pryluky", "Druzhkivka", "Khartsyzk", "Lozova", "Antratsyt", "Stryi", "Kolomyia",
        "Shakhtarsk", "Snizhne", "Novohrad-Volynskyi", "Enerhodar", "Izium", "Myrnohrad", "Brianka", "Chornomorsk", "Boryspil", "Novovolynsk",
        "Rovenky", "Zhovti Vody", "Lubny", "Nova Kakhovka", "Fastiv", "Bilhorod-Dnistrovskyi", "Komsomolsk", "Krasnodon", "Romny", "Okhtyrka",
        "Svitlovodsk", "Marhanets", "Shepetivka", "Pokrov", "Toretsk", "Dzhankoy", "Pervomaisk", "Myrhorod", "Voznesensk", "Kotovsk",
        "Irpin", "Vasylkiv", "Dubno", "Varash", "Volodymyr-Volynskyi", "Kakhovka", "Yuzhnoukrainsk", "Boryslav", "Yasynuvata", "Zhmerynka",
        "Avdiivka", "Chuhuiv", "Sambir", "Tokmak", "Boiarka", "Hlukhiv", "Dobropillia", "Starokostiantyniv", "Kirovsk", "Vyshneve",
        "Netishyn", "Slavuta", "Mohyliv-Podilskyi", "Obukhiv", "Pervomaiskyi", "Kupiansk", "Balakliia", "Synelnykove", "Pereiaslav-Khmelnytskyi", "Alushta",
        "Truskavets", "Krasnoperekopsk", "Kirovske", "Kostopil", "Debaltseve", "Perevalsk", "Saky", "Znamianka", "Ternivka", "Pershotravensk",
        "Khust", "Chortkiv", "Lebedyn", "Zolotonosha", "Bucha", "Novyi Rozdil", "Lyman", "Sarny", "Malyn", "Khmilnyk",
        "Bakhchisaray", "Selydove", "Berehove", "Kaniv", "Koziatyn", "Novoiavorivske", "Korostyshiv", "Popasna", "Vynohradiv", "Haisyn",
        "Molodohvardiysk", "Krolevets", "Merefa", "Volnovakha", "Zdolbuniv", "Kreminna", "Slavutych", "Dokuchaievsk", "Liubotyn", "Oleshky",
        "Yuzhne", "Armiansk", "Vilnohirsk", "Yahotyn", "Sukhodilsk", "Zolochiv", "Trostianets", "Brody", "Polonne", "Vyshhorod",
        "Hadiach", "Krasnohrad", "Kilia", "Starobilsk", "Ladyzhyn", "Polohy", "Amvrosiivka", "Kremenets", "Henichesk", "Sokal",
        "Kurakhove", "Dniprorudne", "Volochysk", "Nadvirna", "Dolyna", "Stebnyk", "Vovchansk", "Krasyliv", "Piatykhatky", "Reni",
        "Bakhmach", "Derhachi", "Vatutine", "Kalynivka", "Balta", "Zvenyhorodka", "Zuhres", "Skadovsk", "Svatove", "Shpola",
        "Novoukrainka", "Korsun-Shevchenkivskyi", "Lutuhyne", "Bilohirsk", "Dolynska", "Iziaslav", "Bilopillia", "Bohodukhiv", "Skvyra", "Karlivka",
        "Orikhiv", "Bilozerske", "Zolote", "Yunokomunarivsk", "Pidhorodne", "Rozdilna", "Horodok", "Chervonopartyzansk", "Ilovaisk", "Berezhany",
        "Novohrodivka", "Vuhledar", "Berezan", "Putyvl", "Bolhrad", "Bar", "Svaliava", "Bohuslav", "Huliaipole", "Zmiiv",
        "Ovruch", "Verkhniodniprovsk", "Ochakiv", "Krasnohorivka", "Kivertsi", "Pyriatyn", "Mykolaivka", "Chasiv Yar", "Vilniansk", "Dunaivtsi",
        "Apostolove", "Talne", "Artsyz", "Novyi Buh", "Tulchyn", "Haivoron", "Horodok", "Hola Prystan", "Nosivka", "Zhashkiv",
        "Horodysche", "Vasylivka", "Kamianka-Dniprovska", "Petrovske", "Beryslav", "Snihurivka", "Radomyshl", "Burshtyn", "Rakhiv", "Novhorod-Siverskyi",
        "Kamianka", "Tetiiv", "Mykolaiv", "Ostroh", "Zelenodolsk", "Vakhrusheve", "Khorol", "Storozhynets", "Sudak", "Siversk",
        "Koriukivka", "Biliaivka", "Hirnyk", "Ukrainka", "Nova Odesa", "Horodnia", "Schastia", "Kaharlyk", "Zhdanivka", "Berezne",
        "Terebovlia", "Vynnyky", "Rozhysche", "Yavoriv", "Zhovkva", "Tarascha", "Myronivka", "Bershad", "Ukrainsk", "Zbarazh",
        "Novomyrhorod", "Uzyn", "Svitlodarsk", "Soledar", "Bashtanka", "Mala Vyska", "Teplohirsk", "Barvinkove", "Prymorsk", "Mena",
        "Hlobyne", "Hnivan", "Komsomolske", "Ichnia", "Novoazovsk", "Baranivka", "Buchach", "Lokhvytsia", "Schors", "Bobrynets",
        "Nemyriv", "Kobeliaky", "Rodynske", "Chyhyryn", "Bobrovytsia", "Sosnivka", "Zhydachiv", "Yampil", "Mospyne", "Borzna",
        "Shcholkine", "Buryn", "Kamianka-Buzka", "Hrebinka", "Khrystynivka", "Hirske", "Tavriysk", "Borschiv", "Zymohiria", "Khotyn",
        "Illintsi", "Pomichna", "Olevsk", "Kamin-Kashyrskyi", "Tatarbunary", "Pohrebysche", "Maryinka", "Bolekhiv", "Inkerman", "Zinkiv",
        "Khodoriv", "Sniatyn", "Derazhnia", "Liuboml", "Valky", "Novodnistrovsk", "Radyvyliv", "Vuhlehirsk", "Sokyriany", "Verkhivtseve",
        "Zalischyky", "Staryi Krym", "Bilytske", "Pereschepyne", "Andrushivka", "Pustomyty", "Horodenka", "Tysmenytsia", "Tiachiv", "Semenivka",
        "Dubrovytsia", "Kodyma", "Irshava", "Berezivka", "Ananyiv", "Monastyrysche", "Lypovets", "Vylkove", "Radekhiv", "Mostyska",
        "Artemivsk", "Novodruzhesk", "Zavodske", "Alupka", "Horokhiv", "Pryvillia", "Chop", "Zastavna", "Zorynsk", "Tlumach",
        "Teplodar", "Lanivtsi", "Busk", "Korets", "Rohatyn", "Pivdenne", "Dubliany", "Rzhyschiv", "Novoselytsia", "Vorozhba",
        "Kosiv", "Pochaiv", "Rava-Ruska", "Molochansk", "Yaremcha", "Turka", "Kitsman", "Peremyshliany", "Blahovishchenske", "Seredyna-Buda",
        "Zboriv", "Khorostkiv", "Oster", "Sharhorod", "Perechyn", "Oleksandrivsk", "Kopychyntsi", "Skole", "Artemove", "Sudova Vyshnia",
        "Halych", "Morshyn", "Monastyryska", "Miusynsk", "Vashkivtsi", "Velyki Mosty", "Druzhba", "Staryi Sambir", "Shumsk", "Sviatohirsk",
        "Almazna", "Vyzhnytsia", "Maydanets", "Dobromyl", "Rudky", "Khyriv", "Skalat", "Komarno", "Bibrka", "Novyi Kalyniv",
        "Hlyniany", "Pidhaitsi", "Baturyn", "Belz", "Ustyluh", "Hertsa", "Berestechko", "Uhniv", "Chernobyl", "Prypiat",
        "Broshniv-Osada", "Svarychiv"
    };
//</editor-fold>
}
