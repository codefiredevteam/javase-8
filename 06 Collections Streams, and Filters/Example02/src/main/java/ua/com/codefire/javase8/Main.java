/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import ua.com.codefire.javase8.models.Cat;
import ua.com.codefire.javase8.models.Dog;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        
        Random rnd = new Random();

        String[] names = {
            "Wonderful", "Yummers", "Zany",
            "Pussy Cat", "Queenie", "Rum-rum",
            "Sweety", "Twinkle Toes", "Uncle Upright",
            "Guy", "Hunny Pot", "Indian Warrior",
            "Juliet", "Knockout", "Luv Puppies",
            "Apple of My Eye", "Button", "Cutie Pootie",
            "Dumpling", "Eyecandy", "Funny Hunny",
            "My World", "Num Nums", "Other Half"
        };
        
        Arrays.stream(names).filter(n -> n.charAt(0) < 'K').forEach(System.err::println);
        
        List<Cat> cats = Arrays.stream(names)
                .filter(nick -> nick.charAt(0) % 2 == 0)
                .map(n -> new Cat(n, rnd.nextInt(15) + 1))
                .collect(Collectors.toList());
        
        cats.forEach(System.out::println);
        
        Arrays.stream(names)
                .filter(nick -> nick.matches("^[BCIJMNOY].+"))
                .map(nick -> new Dog(nick, rnd.nextInt(19) + 1))
                .forEach(System.out::println);
    }
}
