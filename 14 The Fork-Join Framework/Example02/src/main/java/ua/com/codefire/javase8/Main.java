/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import java.util.concurrent.ForkJoinPool;
import ua.com.codefire.javase8.models.StreamTask;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Acailable Processors: " + Runtime.getRuntime().availableProcessors());

        final int componentValue = 1000000;
        Long beginT = System.nanoTime();

        ForkJoinPool fjPool = new ForkJoinPool();
        StreamTask test = new StreamTask(componentValue, 0, componentValue);
        
        fjPool.invoke(test);

        Long endT = System.nanoTime();
        Long timebetweenStartEnd = endT - beginT;
        System.out.println("=====time========" + timebetweenStartEnd);
    }
}
