/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

import java.util.concurrent.RecursiveAction;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class StreamTask extends RecursiveAction {

    private final int countProcessors = Runtime.getRuntime().availableProcessors();
    private final int countLimit = 50;
    private int start;
    private int end;
    private int forSplit;

    public StreamTask(int componentValue, int startNumber, int endNumber) {
        forSplit = componentValue;
        start = startNumber;
        end = endNumber;
    }

    @Override
    protected void compute() {
        if (countProcessors == 1 || end - start <= countLimit) {
//            System.out.println("=run=");
            for (int i = start; i <= end; i++) {
                new Calc().go(i);
            }
        } else {
            int middle = (start + end) / 2;
            invokeAll(new StreamTask(forSplit, 0, middle), new StreamTask(forSplit, middle + 1, end));
        }
    }
}
