/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

import java.util.concurrent.RecursiveTask;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Fibonacci extends RecursiveTask<Integer> {

    private final int n;

    public Fibonacci(int n) {
        this.n = n;
    }

    @Override
    protected Integer compute() {
        if (n <= 1) {
            return n;
        }
        
        Fibonacci subFib1 = new Fibonacci(n - 1);
        subFib1.fork();
        
        Fibonacci subFib2 = new Fibonacci(n - 2);
        
        return subFib2.compute() + subFib1.join();
    }
}
