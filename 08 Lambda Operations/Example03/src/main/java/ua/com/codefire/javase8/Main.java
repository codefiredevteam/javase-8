/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8;

import ua.com.codefire.javase8.model.Employee;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        Random rnd = new Random();

        Consumer<Employee> bousPlus = (e) -> {
            e.setBonus(e.getHours() > 40. ? (e.getHours() - 40.) * (e.getHourRate() * 2.) : .0);
        };

        List<Employee> collect = Arrays.asList("Pupkin", "Vaskin", "Mishin").stream()
                .map(Employee::new)
                .peek((Employee e) -> e.setHourRate(20))
                .peek((Employee e) -> e.setHours(rnd.ints(1, 30, 60).findFirst().getAsInt()))
                .peek(bousPlus)
                .collect(Collectors.toList());

        collect.forEach(System.out::println);
    }

    static void display(Supplier<Integer> arg) {
        System.out.println(arg.get());
    }
}
