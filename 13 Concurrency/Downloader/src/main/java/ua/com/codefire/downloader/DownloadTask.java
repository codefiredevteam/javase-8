/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.downloader;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class DownloadTask implements Runnable {

    private String address;

    public DownloadTask(String address) {
        this.address = address;
    }

    @Override
    public void run() {
        System.out.println(address);
        // TODO: Burn you code here for downloading...
        try {
            URLConnection conn = new URL(address).openConnection();
            String contentType = conn.getContentType();
            
            URL downloadURL = conn.getURL();
            String path = URLDecoder.decode(downloadURL.getPath(), "UTF-8");
            Path localFile = Paths.get("./").normalize().resolve(Paths.get(path).getFileName());
            
            System.out.printf("Download: (%s)\n  %s [%d bytes]\n", downloadURL, contentType, conn.getContentLengthLong());
            
            Files.copy(conn.getInputStream(), localFile, StandardCopyOption.REPLACE_EXISTING);
            
            System.out.printf("Downloaded: (%s)\n  [%s]\n", downloadURL, localFile);
        } catch (IOException ex) {
            Logger.getLogger(DownloadTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
