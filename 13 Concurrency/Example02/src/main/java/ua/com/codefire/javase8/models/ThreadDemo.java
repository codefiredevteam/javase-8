/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javase8.models;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class ThreadDemo extends Thread {

    private Thread t;
    private String threadName;

    public ThreadDemo(String name) {
        this.threadName = name;
        System.out.println("Creating " + threadName);
    }

    @Override
    public void run() {
        System.out.println("Running " + threadName);

        for (int i = 4; i > 0; i--) {
            System.out.println("Thread: " + threadName + ", " + i);

            try {
                // Let the thread sleep for a while.
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("Thread " + threadName + " interrupted.");
            }
        }

        System.out.println("Thread " + threadName + " exiting.");
    }
}
